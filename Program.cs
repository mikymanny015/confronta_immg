﻿using System;

namespace Prova_IMGFILE
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Gestione_FIle gf = new Gestione_FIle();
                int scelta = 0;
                int scelta1 = 0;
                do
                {
                   
                    // 1= png, 2= jpg
                    Console.Clear();
                    Console.WriteLine("0.Esci\n1.Confronta immagini");
                    scelta = Convert.ToInt32(Console.ReadLine());
                    switch (scelta)
                    {
                        case 0:
                            Console.WriteLine("Arrivederci e lasciate la mancia");
                            break;
                        case 1:
                            Console.WriteLine("1.Immagini .png\n2.Immagini .jpg\n3.Immagini .img ");
                            scelta1 = Convert.ToInt32(Console.ReadLine());
                            gf.Img(scelta1);
                            Console.ReadKey();
                            break;
                        default:
                            Console.WriteLine("Valore non valido");
                            Console.ReadKey();
                            break;
                    }

                } while (scelta != 0);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }
    }
}
